import org.junit.Before;
import org.junit.Test;

public class NumerosRomanosTest {

	private Conversor Conversor;
	
	@Before
	public void setup() throws Exception{
		Conversor = new Conversor();
	}
	@Test
	public void testes() {

		assert(Conversor.ArabicoParaRomano(0).compareTo("")==0);
		assert(Conversor.ArabicoParaRomano(1).compareTo("I")==0);
		assert(Conversor.ArabicoParaRomano(2).compareTo("II")==0);
		assert(Conversor.ArabicoParaRomano(3).compareTo("III")==0);
		assert(Conversor.ArabicoParaRomano(4).compareTo("IV")==0);
		assert(Conversor.ArabicoParaRomano(5).compareTo("V")==0);
		assert(Conversor.ArabicoParaRomano(6).compareTo("VI")==0);
		assert(Conversor.ArabicoParaRomano(7).compareTo("VII")==0);
		assert(Conversor.ArabicoParaRomano(8).compareTo("VIII")==0);
		assert(Conversor.ArabicoParaRomano(9).compareTo("IX")==0);
		assert(Conversor.ArabicoParaRomano(10).compareTo("X")==0);
		assert(Conversor.ArabicoParaRomano(11).compareTo("XI")==0);
		assert(Conversor.ArabicoParaRomano(12).compareTo("XII")==0);
		assert(Conversor.ArabicoParaRomano(13).compareTo("XIII")==0);
		assert(Conversor.ArabicoParaRomano(14).compareTo("XIV")==0);
		assert(Conversor.ArabicoParaRomano(15).compareTo("XV")==0);	
		assert(Conversor.ArabicoParaRomano(16).compareTo("XVI")==0);
		assert(Conversor.ArabicoParaRomano(17).compareTo("XVII")==0);
		assert(Conversor.ArabicoParaRomano(18).compareTo("XVIII")==0);
		assert(Conversor.ArabicoParaRomano(20).compareTo("XX")== 0);
		assert(Conversor.ArabicoParaRomano(23).compareTo("XXIII")== 0);
		assert(Conversor.ArabicoParaRomano(34).compareTo("XXXIV")==0);
		assert(Conversor.ArabicoParaRomano(39).compareTo("XXXIV")==0);
		assert(Conversor.ArabicoParaRomano(46).compareTo("XXXIV")==0);
		assert(Conversor.ArabicoParaRomano(59).compareTo("XXXIV")==0);
		assert(Conversor.ArabicoParaRomano(63).compareTo("XXXIV")==0);
		assert(Conversor.ArabicoParaRomano(90).compareTo("XC")==0);
		assert(Conversor.ArabicoParaRomano(94).compareTo("XCIV")==0);
		assert(Conversor.ArabicoParaRomano(99).compareTo("XCIX")==0);
		assert(Conversor.ArabicoParaRomano(103).compareTo("CII")==0);
		assert(Conversor.ArabicoParaRomano(149).compareTo("CXLIX")==0);
		assert(Conversor.ArabicoParaRomano(349).compareTo("CCCXLIX")==0);
		
		
		System.out.println("Fim dos testes");
	}
	
	public static void main(String args[]){
	NumerosRomanosTest t = new NumerosRomanosTest();
	t.testes();
}
}