
public class Conversor {
	
	private int v[] = {100, 50, 10, 5, 1};
	private char corresp[] = {'C', 'L', 'X', 'V', 'I'};
	
	private int colocaLetra(int x, int i, StringBuffer s){
		
			s.append(corresp[i]);
			x = x - v[i];
		
		return x;
	}
	
	private int precede(int x, int i, StringBuffer s){
		
		s.append(corresp[i]);
		return x + v[i];
	}		
	
	String ArabicoParaRomano(int arabico){
	
		StringBuffer s = new StringBuffer("");
		int i = 0;
		
		while (i < v.length - 1){
			while (arabico >= v[i] - v[i+2]){
				if (arabico < v[i]){
					arabico = precede(arabico, i+2, s);
				}
				arabico = colocaLetra(arabico, i, s);
			}
			while (arabico >= v[i+1] - v[i+2]){
				if(arabico < v[i+1]){
					arabico = precede(arabico, i+2, s);
				}
				arabico = colocaLetra(arabico, i+1, s);
			}
			i = i + 2;
		}
		
		while (arabico > 0){
			arabico = colocaLetra(arabico, v.length - 1, s);
		}
		return s.toString();
	}
}